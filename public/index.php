<?php

use LocalExpress\Processing\Bootstrap\Core\Bootstrap as Application;

# workaround for first daemon initialize
# sleep(1);
require_once dirname(__DIR__) . '/vendor/autoload.php';
(new Application)->run();

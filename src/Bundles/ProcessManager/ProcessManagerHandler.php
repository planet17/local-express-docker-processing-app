<?php

namespace LocalExpress\Processing\Bundles\ProcessManager;

use LocalExpress\CommonQueueObjects\Bundles\ProcessManager\Messages\ProcessManagerMessage;
use LocalExpress\CommonQueueObjects\Bundles\ProcessManager\Routes\ProcessManagerRoute;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessageInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteInterface;
use Planet17\MessageQueueProcessManager\DTO\ProcessManagerDTO;

/**
 * Class Handler
 *
 * @package LocalExpress\Processing\Queue\Gearman\ApplicationCore
 */
class ProcessManagerHandler extends \Planet17\ApplicationProcessManagerRoutedQueue\Handlers\ProcessManagerHandler
{
    /** @var string|RouteInterface $routeClass */
    protected $routeClass = ProcessManagerRoute::class;

    /** @inheritdoc */
    protected function makeSelfMessage(ProcessManagerDTO $dto): MessageInterface
    {
        return new ProcessManagerMessage($dto);
    }
}

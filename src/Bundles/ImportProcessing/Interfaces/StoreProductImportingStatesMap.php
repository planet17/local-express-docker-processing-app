<?php

namespace LocalExpress\Processing\Bundles\ImportProcessing\Interfaces;

/**
 * Interface StoreProductImportingStatesMap
 *
 * @package LocalExpress\Processing\Bundles\ImportProcessing\Interfaces
 */
interface StoreProductImportingStatesMap
{
    /**
     * @const FILE_PROCESSING_STATE_NEW
     */
    public const IMPORT_STATE_NEW = 1;

    /**
     * @const FILE_PROCESSING_STATE_PROCESSING
     */
    public const IMPORT_STATE_PROCESSING = 2;

    /**
     * @const FILE_PROCESSING_STATE_DONE
     */
    public const IMPORT_STATE_DONE = 3;
}

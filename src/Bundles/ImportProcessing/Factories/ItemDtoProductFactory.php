<?php

namespace LocalExpress\Processing\Bundles\ImportProcessing\Factories;

use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ProductDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\ProductPriceFreeException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\ProductPriceInvalidException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\ProductPriceRequiredException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\ProductUpcRequiredException;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\ItemRowDtoFactoryInterface;

/**
 * Class ProductDtoFactory
 *
 * @package LocalExpress\Processing\Bundles\ImportProcessing\Factories
 */
class ItemDtoProductFactory implements ItemRowDtoFactoryInterface
{
    private const ATTR_NAME_UPC = 'upc';
    private const ATTR_NAME_PRICE = 'price';
    private const ATTR_NAME_TITLE = 'title';

    /**
     * @inheritDoc
     *
     * @return ProductDto
     *
     * @throws ProductUpcRequiredException
     * @throws ProductPriceRequiredException
     * @throws ProductPriceInvalidException
     * @throws ProductPriceFreeException
     *
     * @noinspection ReturnTypeCanBeDeclaredInspection
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function makeFrom(array $data)
    {
        if (!array_key_exists(self::ATTR_NAME_UPC, $data) || !$data[self::ATTR_NAME_UPC]) {
            throw new ProductUpcRequiredException;
        }

        if (!array_key_exists(self::ATTR_NAME_PRICE, $data) || !$data[self::ATTR_NAME_PRICE]) {
            throw new ProductPriceRequiredException;
        }

        if (!is_numeric($data[self::ATTR_NAME_PRICE])) {
            throw new ProductPriceInvalidException;
        }

        if ($data[self::ATTR_NAME_PRICE] <= 0) {
            throw new ProductPriceFreeException;
        }

        return new ProductDto(
            $data[self::ATTR_NAME_UPC],
            $data[self::ATTR_NAME_PRICE],
            $data[self::ATTR_NAME_TITLE] ?? null
        );
    }
}

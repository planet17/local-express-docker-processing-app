<?php

namespace LocalExpress\Processing\Bundles\ImportProcessing\Routes;

use LocalExpress\CommonQueueObjects\Bundles\Core\Routes\GearmanRoute;

/**
 * Class FileProcessingRoute
 *
 * @package LocalExpress\Processing\Bundles\ImportProcessing\Routes
 */
class FileProcessingRoute extends GearmanRoute
{
    /**
     * @return string
     */
    public function getAliasShort(): string
    {
        return 'file-processing';
    }
}

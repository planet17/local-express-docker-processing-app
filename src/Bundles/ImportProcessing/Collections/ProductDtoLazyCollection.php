<?php

namespace LocalExpress\Processing\Bundles\ImportProcessing\Collections;

use Exception;
use Generator;
use Iterator;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ProductDto;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\LazyCollectionEmptyException;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\LazyCollectionAbstractInterface;

/**
 * Class LazyProductDtoCollection
 *
 * @package LocalExpress\Processing\Bundles\ImportProcessing\Collections
 */
class ProductDtoLazyCollection implements LazyCollectionAbstractInterface
{
    /** @var Generator|null */
    private $generator = null;

    /** @var int|null */
    private $count = null;

    /** @inheritdoc */
    public function set(Iterator $generator, ?int $preCount = null): LazyCollectionAbstractInterface
    {
        $this->reset();
        $this->setPreCount($preCount);
        $this->generator = $generator;

        return $this;
    }

    /**
     * Method reset counter and property contains reference to generator for getting DTO.
     */
    private function reset(): void
    {
        $this->count = null;
        $this->generator = null;
    }

    /**
     * @return Iterator
     */
    protected function getGenerator(): Iterator
    {
        if ($this->generator === null) {
            throw new LazyCollectionEmptyException('Collection must been set before use. ');
        }

        return $this->generator;
    }

    /**
     * Method help return object contained from collection while traversing.
     *
     * @return ProductDto|Exception
     */
    public function current()
    {
        return $this->getGenerator()->current();
    }

    /** @inheritdoc */
    public function next(): void
    {
        $this->getGenerator()->next();
    }

    /** @inheritdoc */
    public function key()
    {
        return $this->getGenerator()->key();
    }

    /** @inheritdoc */
    public function valid(): bool
    {
        return $this->getGenerator()->valid();
    }

    /** @inheritdoc */
    public function rewind():void
    {
        $this->getGenerator()->rewind();
    }

    /** @inheritdoc */
    public function count(): int
    {
        if ($this->count === null) {
            $this->getGenerator()->rewind();
            $this->count = iterator_count($this->getGenerator());
            $this->getGenerator()->rewind();
        }

        return $this->count;
    }

    /**
     * Method for setting count whether it know while provide data items to collection.
     *
     * @param int|null $preCount
     */
    private function setPreCount(?int $preCount): void
    {
        if ($preCount !== 0 && $preCount > 0) {
            $this->count = $preCount;
        }
    }
}

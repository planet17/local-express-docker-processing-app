<?php

namespace LocalExpress\Processing\Bundles\ImportProcessing\DTO;

/**
 * Class FileProcessingTaskDto
 *
 * @package LocalExpress\Processing\Bundles\ImportProcessing\DTO
 */
class FileProcessingTaskDto
{
    /** @var int $importIdentifier */
    private $importIdentifier;

    /** @var int $storeIdentifier */
    private $storeIdentifier;

    /** @var string $filePath */
    private $filePath;

    /**
     * FileProcessingTaskDto constructor.
     *
     * @param int $importIdentifier
     * @param int $storeIdentifier
     * @param string $filePath
     */
    public function __construct(int $importIdentifier, int $storeIdentifier, string $filePath)
    {
        $this->importIdentifier = $importIdentifier;
        $this->storeIdentifier = $storeIdentifier;
        $this->filePath = $filePath;
    }

    /**
     * Getter import identifier.
     *
     * @return mixed
     */
    public function getImportIdentifier(): int
    {
        return $this->importIdentifier;
    }

    /**
     * Getter store identifier.
     *
     * @return mixed
     */
    public function getStoreIdentifier(): int
    {
        return $this->storeIdentifier;
    }

    /**
     * Getter full path to file for reading and processing.
     *
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }
}

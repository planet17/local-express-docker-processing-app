<?php

namespace LocalExpress\Processing\Bundles\ImportProcessing\Services;

use Exception;
use Generator;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ImportStateUpdatingDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\InvalidTableException;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\NotFoundRequiredColumnException;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\FileReaderTableLazyInterface;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\FileReaderTableValidatorInterface;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\ItemRowDtoFactoryInterface;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\LazyCollectionAbstractInterface;

/**
 * Class FileTableExtractorService
 *
 * @package LocalExpress\Processing\Bundles\ImportProcessing\Services
 */
class FileTableExtractorService
{
    /** @var FileReaderTableValidatorInterface */
    private $validator;

    /** @var ItemRowDtoFactoryInterface */
    private $factory;

    /** @var FileReaderTableLazyInterface */
    private $fileReader;

    /**
     * FileTableExtractorService constructor.
     *
     * @param FileReaderTableValidatorInterface $validator
     * @param $factory
     * @param FileReaderTableLazyInterface $fileReader
     */
    public function __construct(
        FileReaderTableValidatorInterface $validator,
        ItemRowDtoFactoryInterface $factory,
        FileReaderTableLazyInterface $fileReader
    ) {
        $this->validator = $validator;
        $this->factory = $factory;
        $this->fileReader = $fileReader;
    }

    /**
     * Main core method.
     *
     * @param LazyCollectionAbstractInterface $collection Object for collecting items.
     * @param string $filePath
     * @param ImportStateUpdatingDto $dto
     */
    public function handle(
        LazyCollectionAbstractInterface $collection,
        string $filePath,
        ImportStateUpdatingDto $dto
    ): void {
        $this->fileReader->setFlagSeparateHeaders(true)->handleFile($filePath);
        try {
            $headers = $this->validator->validate($this->fileReader)->validatedColumns();
            $dto->setCountTotalProducts($this->fileReader->countRows());
        } catch (NotFoundRequiredColumnException $exception) {
            throw new InvalidTableException($this->fileReader->countRows(), 0, $exception);
        }

        $generator = $this->fileReader->getMappedRows(...$headers);
        $collection->set($this->processingItems($generator), $this->fileReader->countRows());
    }

    /**
     * Make like generator traversing, validate, making DTO with all items.
     *
     * @param Generator|null $generator
     *
     * @return Generator|null
     */
    public function processingItems(?Generator $generator): ?Generator
    {
        if (!$generator) {
            return null;
        }

        foreach ($generator as $item) {
            try {
                if ($item instanceof Exception) {
                    throw new $item;
                }

                $this->validator->validateRow($item);
                yield $this->factory->makeFrom($item);
            } catch (Exception $exception) {
                yield $exception;
            }
        }
    }
}

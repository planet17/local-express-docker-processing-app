<?php

namespace LocalExpress\Processing\Bundles\ImportProcessing\Validation;

use LocalExpress\Processing\Bundles\FileReader\Validation\Rules;

/**
 * Class ProductRules
 *
 * @package LocalExpress\Processing\Bundles\ImportProcessing\Validation
 */
class ProductRules extends Rules
{
    /** @const FIELD_NAME_UPC string */
    public const FIELD_NAME_UPC = 'upc';

    /** @const FIELD_NAME_PRICE string */
    public const FIELD_NAME_PRICE = 'price';

    /** @const FIELD_NAME_TITLE string */
    public const FIELD_NAME_TITLE = 'title';

    /** @inheritdoc */
    public function getColumnsRequired(): array
    {
        return [
            self::FIELD_NAME_UPC,
            self::FIELD_NAME_PRICE
        ];
    }

    /** @inheritdoc */
    public function getColumnsOptional(): array
    {
        return [
            self::FIELD_NAME_TITLE
        ];
    }
}

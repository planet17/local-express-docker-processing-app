<?php

namespace LocalExpress\Processing\Bundles\ImportProcessing\Handlers;

use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer\ConfigurationDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer\TaskHadBeenProcessedDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer\TaskNewDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages\ImportProcessingBalancerMessage;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes\ImportProcessingBalancerRoute;
use LocalExpress\CommonQueueObjects\Bundles\ProcessManager\Messages\ProcessManagerMessage;
use LocalExpress\Processing\Bundles\ImportProcessing\DTO\FileProcessingTaskDto;
use LocalExpress\Processing\Bundles\ImportProcessing\Messages\ApplicationCallbackOnInitMessage;
use LocalExpress\Processing\Bundles\ImportProcessing\Messages\FileProcessingMessage;
use LocalExpress\Processing\Bundles\ImportProcessing\Routes\FileProcessingRoute;
use Planet17\MessageQueueLibraryPipelineHandlers\Handlers\PipelinedHandler;
use Planet17\MessageQueueProcessManager\DTO\ProcessManagerDTO;

/**
 * Class ImportProcessingBalancerHandler
 *
 * @package LocalExpress\Processing\Bundles\ImportProcessing\Handlers
 */
class ImportProcessingBalancerHandler extends PipelinedHandler
{
    /** @var string $routeClass */
    protected $routeClass = ImportProcessingBalancerRoute::class;

    /** @var int $amountActiveHandlerProcessing */
    private $amountActiveHandlerProcessing = 0;

    /** @var int $busyHandlerProcessingAmount */
    private $busyHandlerProcessingAmount = 0;

    /** @var TaskNewDto[] $queue */
    private $queue = [];

    /** @var TaskNewDto[] $queueProcessing */
    private $queueProcessing = [];

    /** @var TaskNewDto[] $processingFileStoreActive */
    private $processingFileStoreActive = [];

    /** @var int */
    private $amountDefaultProcessingParallelImportPerCompany = 1;

    /** @var array Memory of had been processed import */
    private $skip = [];

    /** @var bool */
    private $hasQueueNonChecked;

    /**
     * Override it for additional commands.
     */
    public function initialize(): void
    {
        $this->sendFetchingJobCommand();
        parent::initialize();
    }

    /**
     * Method send signal to Application for sending all hasn't been processed imports.
     */
    private function sendFetchingJobCommand(): void
    {
        $this->next(null, ApplicationCallbackOnInitMessage::class);
    }

    /**
     * Implement handle common.
     *
     * At each step it is looking for new tasks what could be started.
     *
     * @param ImportProcessingBalancerMessage $payload
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function handle($payload): void
    {
        $this->hasQueueNonChecked = true;

        $dto = $payload->getDto();
        switch (true) {
            case $dto instanceof ConfigurationDto:
                $this->handleConfiguration($dto);
                break;
            case $dto instanceof TaskNewDto:
                $this->handleImport($dto);
                break;
            case $dto instanceof TaskHadBeenProcessedDto:
                $this->handleImportHadBeenProcessed($dto);
                break;
        }

        while ($this->isCouldBeFoundFreeTask()) {
            $this->resolveProcessingNext();
        }
    }

    /**
     * Method check free handlers, free tasks and know does all tasks has been checked with conditions.
     *
     * @return bool
     */
    private function isCouldBeFoundFreeTask(): bool
    {
        /** @var $hasFreeHandlers */
        $hasFreeHandlers = $this->busyHandlerProcessingAmount < $this->amountActiveHandlerProcessing;
        $isNotEmptyQueue = !empty($this->queue);

        return $hasFreeHandlers && $isNotEmptyQueue && $this->hasQueueNonChecked;
    }

    /**
     * Method handle incoming message with set up new params to the Handler.
     *
     * @param ConfigurationDto $dto
     */
    private function handleConfiguration(ConfigurationDto $dto): void
    {
        $this->amountActiveHandlerProcessing = $dto->getAmountActiveHandlerProcessing();
        $this->amountDefaultProcessingParallelImportPerCompany = $dto
            ->getAmountDefaultProcessingParallelImportPerCompany();

        $alias = FileProcessingRoute::getInstance()->getAliasFull();
        $dtoNext = new ProcessManagerDTO($alias, $this->amountActiveHandlerProcessing);
        $this->next($dtoNext, ProcessManagerMessage::class);
    }

    /**
     * Push new task to logical queue.
     *
     * @param TaskNewDto $dto
     */
    private function handleImport(TaskNewDto $dto): void
    {
        if (array_key_exists($dto->getImportIdentifier(), $this->skip)) {
            echo 'Invalid task provided - duplicate for task: ' . $dto->getImportIdentifier() . PHP_EOL;
            return;

        }

        $this->skip[$dto->getImportIdentifier()] = null;
        $this->queue[] = $dto;
    }

    /**
     * Method cut item from list of queueProcessing and decrementing counters.
     *
     * @param TaskHadBeenProcessedDto $dto
     */
    private function handleImportHadBeenProcessed(TaskHadBeenProcessedDto $dto): void
    {
        if (!array_key_exists($dto->getImportIdentifier(), $this->queueProcessing)) {
            return;
        }

        unset($this->queueProcessing[$dto->getImportIdentifier()]);

        $this->busyHandlerProcessingAmount--;
        $this->processingFileStoreActive[$dto->getStoreIdentifier()]--;
    }

    /**
     * Method for figured out new available for processing task.
     */
    private function resolveProcessingNext(): void
    {
        $this->researchCountingProcessingFilesByStores();
        $this->findTaskAllowedToRun();
    }

    /**
     * Method created mapped counters by store for correct finding with store strict.
     *
     * @see ImportProcessingBalancerHandler::findTaskAllowedToRun() - work in pair with it.
     */
    private function researchCountingProcessingFilesByStores(): void
    {
        foreach ($this->queueProcessing as $dto) {
            if (!array_key_exists($dto->getStoreIdentifier(), $this->processingFileStoreActive)) {
                $this->processingFileStoreActive[$dto->getStoreIdentifier()] = 0;
            }

            $this->processingFileStoreActive[$dto->getStoreIdentifier()]++;
        }
    }

    /**
     * Method strategy found next available DTO passes the store parallels processing strict.
     *
     * @see ImportProcessingBalancerHandler::researchCountingProcessingFilesByStores() - work in pair with it.
     */
    private function findTaskAllowedToRun(): void
    {
        foreach ($this->queue as $key => $dto) {
            $limit = $dto->getStoreProcessingParallelLimit() ?? $this->amountDefaultProcessingParallelImportPerCompany;
            if (
                array_key_exists($dto->getStoreIdentifier(), $this->processingFileStoreActive)
                && ($limit >= $this->processingFileStoreActive[$dto->getStoreIdentifier()])
            ) {
                continue;
            }

            $this->appendToTheQueueProcessing($dto);
            $this->sendFileProcessingTask($dto);
            unset($this->queue[$key]);
            break;
        }

        $this->hasQueueNonChecked = false;
    }

    /**
     * Method push new item to list of processing and iterate counters.
     *
     * @param TaskNewDto $dto
     */
    private function appendToTheQueueProcessing(TaskNewDto $dto): void
    {
        $this->queueProcessing[$dto->getImportIdentifier()] = $dto;
        $this->busyHandlerProcessingAmount++;
    }

    /**
     * Fill new dto and call PipelinedHandler::next().
     *
     * @param TaskNewDto $dto
     */
    private function sendFileProcessingTask(TaskNewDto $dto): void
    {
        $this->next(
            new FileProcessingTaskDto(
                $dto->getImportIdentifier(),
                $dto->getStoreIdentifier(),
                $dto->getFilePath()
            ),
            FileProcessingMessage::class
        );
    }
}

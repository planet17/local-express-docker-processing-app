<?php

namespace LocalExpress\Processing\Bundles\ImportProcessing\Handlers;

use Exception;
use InvalidArgumentException;
use Iterator;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\Balancer\TaskHadBeenProcessedDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ImportProductStoringDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ImportStateUpdatingDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ProductDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ProductMakingExceptionDto;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\FileReadingException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\InvalidTableException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\ProductDtoMakingException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\ProductPriceFreeException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\ProductPriceInvalidException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\ProductPriceRequiredException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Exceptions\ProductUpcRequiredException;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages\ImportProcessingBalancerMessage;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages\ImportProductStoringMessage;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Messages\ImportStateUpdatingMessage;
use LocalExpress\Processing\Bundles\FileReader\Readers\FileReaderTableLazy;
use LocalExpress\Processing\Bundles\FileReader\Validation\Validator;
use LocalExpress\Processing\Bundles\ImportProcessing\Collections\ProductDtoLazyCollection;
use LocalExpress\Processing\Bundles\ImportProcessing\DTO\FileProcessingTaskDto;
use LocalExpress\Processing\Bundles\ImportProcessing\Factories\ItemDtoProductFactory;
use LocalExpress\Processing\Bundles\ImportProcessing\Interfaces\StoreProductImportingStatesMap;
use LocalExpress\Processing\Bundles\ImportProcessing\Messages\FileProcessingMessage;
use LocalExpress\Processing\Bundles\ImportProcessing\Routes\FileProcessingRoute;
use LocalExpress\Processing\Bundles\ImportProcessing\Services\FileTableExtractorService;
use LocalExpress\Processing\Bundles\ImportProcessing\Validation\ProductRules;
use Planet17\MessageQueueLibraryPipelineHandlers\Handlers\PipelinedHandler;

/**
 * Class FileProcessingHandler
 *
 * @package LocalExpress\Processing\Bundles\ImportProcessing\Handlers
 */
class FileProcessingHandler extends PipelinedHandler
{
    protected $routeClass = FileProcessingRoute::class;

    /** @var ImportStateUpdatingDto */
    private $statefulDto;

    /** @var ItemDtoProductFactory $factory */
    private $factory;

    /** @var FileTableExtractorService $service */
    private $service;

    /** @var string */
    private $directoryPathCdn;

    /**
     * FileProcessingHandler constructor.
     */
    public function __construct()
    {
        $this->factory = new ItemDtoProductFactory;
        $this->directoryPathCdn = getenv('DIR_CDN', '/cdn');

        $this->service = new FileTableExtractorService(
            new Validator(new ProductRules),
            $this->factory,
            new FileReaderTableLazy
        );
    }

    /**
     * Implement handler.
     *
     * @param mixed|FileProcessingMessage $payload
     *
     * @noinspection PhpMissingParamTypeInspection
     */
    public function handle($payload): void
    {
        $this->refreshStatefulDto($payload->getDto());
        $collection = $this->makeNewCollection();

        try {
            /* Get file, scan it, validate and prepare generator, send state about processing. */
            $filePath = $this->directoryPathCdn . $payload->getDto()->getFilePath();
            $this->service->handle($collection, $filePath, $this->statefulDto);
            /* Send it after counting all products by handler. */
            $this->sendOnProcessingStart();

            /* Start cycle for processing each product. */
            foreach ($collection as $item) {
                $this->next(
                    new ImportProductStoringDto(
                        $payload->getDto()->getImportIdentifier(),
                        $payload->getDto()->getStoreIdentifier(),
                        $this->prepareItem($item)
                    ),
                    ImportProductStoringMessage::class
                );
            }
        } catch (InvalidTableException $exception) {
            $this->statefulDto->setException($exception);
            if ($exception->getCountFailedRows()) {
                $this->statefulDto->setCountTotalProducts($exception->getCountFailedRows());
            }
        } catch (Exception $exception) {
            $this->statefulDto
                ->setException(
                    new FileReadingException(
                        $exception->getMessage(),
                        $exception->getCode(),
                        $exception
                    )
                );
        }

        $this->sendOnProcessingDone();
    }

    /** @return Iterator */
    private function makeNewCollection()
    {
        return new ProductDtoLazyCollection();
    }

    /**
     * Method send message for update state of processing.
     */
    private function makeMessageOnProcessingStateChange(): void
    {
        $this->next($this->statefulDto, ImportStateUpdatingMessage::class);
    }

    /**
     * Method wrapper for sending message on receiving task by handler.
     */
    private function sendOnProcessingStart(): void
    {
        $this->statefulDto->setState(StoreProductImportingStatesMap::IMPORT_STATE_PROCESSING);
        $this->makeMessageOnProcessingStateChange();
    }

    /**
     * Method wrapper for sending message on failing and on completing processing.
     */
    private function sendOnProcessingDone(): void
    {
        $this->statefulDto->setState(StoreProductImportingStatesMap::IMPORT_STATE_DONE);
        $this->makeMessageOnProcessingStateChange();
        $this->makeMessageToBalancer();
    }

    /**
     * Signal to previous about finally: it is like "I am free now".
     */
    private function makeMessageToBalancer(): void
    {
        $this->next(
            new TaskHadBeenProcessedDto(
                $this->statefulDto->getImportIdentifier(),
                $this->statefulDto->getStoreIdentifier()
            ),
            ImportProcessingBalancerMessage::class
        );
    }

    /**
     * Method reset property with new object (changeable DTO).
     *
     * It must called above new queue task/job incoming.
     *
     * @param FileProcessingTaskDto $dto
     */
    private function refreshStatefulDto(FileProcessingTaskDto $dto): void
    {
        $this->statefulDto = new ImportStateUpdatingDto(
            $dto->getImportIdentifier(),
            $dto->getStoreIdentifier(),
            null,
            StoreProductImportingStatesMap::IMPORT_STATE_NEW,
            null
        );
    }

    /**
     * Method check help check type of item.
     *
     * Whether it is unknown it will change it.
     *
     * @param mixed|ProductMakingExceptionDto|ProductDto $item
     *
     * @return mixed|ProductMakingExceptionDto|ProductDto
     */
    private function prepareItem($item)
    {
        if ($item instanceof ProductDto) {
            return $item;
        }

//        if ($item instanceof ProductUpcRequiredException) {
//            return $item;
//        }
//
//        if ($item instanceof ProductPriceRequiredException) {
//            return $item;
//        }
//
//        if ($item instanceof ProductPriceInvalidException) {
//            return $item;
//        }
//
//        if ($item instanceof ProductPriceFreeException) {
//            return $item;
//        }

        if ($item instanceof Exception) {
            return new ProductMakingExceptionDto($item->getMessage());
        }

        throw new InvalidArgumentException('Undefined type of item');
    }
}

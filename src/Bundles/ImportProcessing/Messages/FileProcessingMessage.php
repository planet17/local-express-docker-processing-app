<?php

namespace LocalExpress\Processing\Bundles\ImportProcessing\Messages;

use LocalExpress\Processing\Bundles\ImportProcessing\DTO\FileProcessingTaskDto;
use LocalExpress\Processing\Bundles\ImportProcessing\Routes\FileProcessingRoute;
use Planet17\MessageQueueLibrary\Messages\BaseMessage;
use Planet17\MessageQueueProcessManager\Exception\WrongDTOProvidedException;

/**
 * Class FileProcessingMessage
 *
 * @package LocalExpress\Processing\Bundles\ImportProcessing\Messages
 */
class FileProcessingMessage extends BaseMessage
{
    protected $routeClass = FileProcessingRoute::class;

    /** @var FileProcessingTaskDto */
    private $dto;

    /**
     * Message constructor.
     *
     * @param null|FileProcessingTaskDto $payload
     *
     * @throws WrongDTOProvidedException
     *
     * @noinspection MagicMethodsValidityInspection
     * @noinspection PhpMissingParentConstructorInspection
     */
    public function __construct($payload = null)
    {
        if ($payload !== null && !($payload instanceof FileProcessingTaskDto)) {
            throw new WrongDTOProvidedException;
        }

        $this->dto = $payload;
    }

    /**
     * Getter for DTO.
     *
     * @return FileProcessingTaskDto
     */
    public function getDto(): FileProcessingTaskDto
    {
        return $this->dto;
    }

    /**
     * Override.
     *
     * @return string
     */
    public function getPayload(): string
    {
        return serialize($this);
    }
}

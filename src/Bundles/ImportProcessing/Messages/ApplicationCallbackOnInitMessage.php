<?php

namespace LocalExpress\Processing\Bundles\ImportProcessing\Messages;

use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes\ApplicationCallbackOnInitRoute;
use Planet17\MessageQueueLibrary\Messages\BaseMessage;

/**
 * Class ApplicationCallbackOnInitMessage
 *
 * @package LocalExpress\Processing\Bundles\ImportProcessing\Messages
 */
class ApplicationCallbackOnInitMessage extends BaseMessage
{
    protected $routeClass = ApplicationCallbackOnInitRoute::class;
}

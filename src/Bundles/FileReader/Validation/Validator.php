<?php

namespace LocalExpress\Processing\Bundles\FileReader\Validation;

use LocalExpress\Processing\Bundles\FileReader\Exceptions\NotFoundAnyAllowedParamException;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\NotFoundRequiredColumnException;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\NotFoundRequiredParamException;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\TableWithoutHeadersCouldNotBeenValidatedException;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\FileReaderTableLazyInterface;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\FileReaderTableValidatorInterface;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\FileReaderTableValidatorRulesInterface;

/**
 * Class Validation
 *
 * @package LocalExpress\Processing\Bundles\FileReader
 */
class Validator implements FileReaderTableValidatorInterface
{
    /** @var FileReaderTableValidatorRulesInterface $rules */
    private $rules;

    /** @var array $valid */
    protected $valid = [];

    /** @inheritdoc */
    public function __construct(FileReaderTableValidatorRulesInterface $rules)
    {
        $this->rules = $rules;
    }

    /** @inheritdoc */
    public function getRules():FileReaderTableValidatorRulesInterface
    {
        return $this->rules;
    }

    /** @inheritdoc */
    public function validate(FileReaderTableLazyInterface $reader): FileReaderTableValidatorInterface
    {
        if (!$reader->hasHeaders()) {
            throw new TableWithoutHeadersCouldNotBeenValidatedException('No headers.');
        }

        return $this->validateColumns($reader->getHeaders());
    }

    /**
     * Method validate columns with required by rules.
     *
     * @param $columns
     *
     * @return $this|FileReaderTableValidatorInterface
     */
    protected function validateColumns($columns): FileReaderTableValidatorInterface
    {

        foreach ($this->getRules()->getColumnsRequired() as $column) {
            if (!in_array($column, $columns, true)) {
                throw new NotFoundRequiredColumnException($column);
            }
        }

        foreach ($this->getRules()->getColumnsAllowed() as $column) {
            if (in_array($column, $columns, true)) {
                $this->valid[] = $column;
            }
        }

        return $this;
    }

    /** @inheritdoc */
    public function validateRow(array $row): FileReaderTableValidatorInterface
    {
        foreach ($this->getRules()->getColumnsRequired() as $param) {
            if (!($row[$param] ?? null)) {
                throw new NotFoundRequiredParamException($param);
            }
        }

        return $this;
    }

    /** @inheritdoc */
    public function validatedColumns(): array
    {
        if (!$this->valid) {
            throw new NotFoundAnyAllowedParamException('Has no allowed columns');
        }

        return $this->valid;
    }
}

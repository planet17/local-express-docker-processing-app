<?php

namespace LocalExpress\Processing\Bundles\FileReader\Validation;

use LocalExpress\Processing\Bundles\FileReader\Interfaces\FileReaderTableValidatorRulesInterface;

/**
 * Class Rules
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Validation
 */
abstract class Rules implements FileReaderTableValidatorRulesInterface
{
    /** @inheritdoc */
    abstract public function getColumnsRequired(): array;

    /** @inheritdoc */
    abstract public function getColumnsOptional(): array;

    /** @inheritdoc */
    final public function getColumnsAllowed(): array
    {
        return array_merge($this->getColumnsRequired(), $this->getColumnsOptional());
    }
}

<?php

namespace LocalExpress\Processing\Bundles\FileReader\Interfaces;

/**
 * Class FileReaderInterface
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Interfaces
 */
interface FileReaderBaseInterface
{
    /**
     * Method execute core features, reading file, extract data.
     *
     * @param string $filePath
     *
     * @return void
     */
    public function handleFile(string $filePath): void;
}

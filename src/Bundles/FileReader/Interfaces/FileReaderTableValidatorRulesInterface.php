<?php

namespace LocalExpress\Processing\Bundles\FileReader\Interfaces;

/**
 * Interface FileReaderTableValidatorRulesInterface
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Interfaces
 *
 * @see FileReaderTableValidatorRulesInterface::getColumnsRequired()
 * @see FileReaderTableValidatorRulesInterface::getColumnsOptional()
 * @see FileReaderTableValidatorRulesInterface::getColumnsAllowed()
 */
interface FileReaderTableValidatorRulesInterface
{
    /**
     * Method must implement return of required columns.
     *
     * @return array
     */
    public function getColumnsRequired():array;

    /**
     * Method must implement return of optional columns.
     *
     * @return array
     */
    public function getColumnsOptional():array;

    /**
     * Method must return mix of previous methods.
     *
     * @return array
     *
     * @see FileReaderTableValidatorRulesInterface::getColumnsRequired()
     * @see FileReaderTableValidatorRulesInterface::getColumnsOptional()
     */
    public function getColumnsAllowed():array;
}

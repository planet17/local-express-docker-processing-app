<?php

namespace LocalExpress\Processing\Bundles\FileReader\Interfaces;

use Countable;
use Exception;
use Iterator;
use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ProductDto;

/**
 * Class LazyCollectionAbstractInterface
 *
 * Iterator and Countable interfaces without method Iterator::current() and declared return type for declared methods.
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Interfaces
 */
interface LazyCollectionAbstractInterface extends Iterator, Countable
{
    /**
     * @param Iterator $generator
     * @param int|null $preCount
     *
     * @return LazyCollectionAbstractInterface
     */
    public function set(Iterator $generator, ?int $preCount = null): LazyCollectionAbstractInterface;

    /**
     * Method help return object contained from collection while traversing.
     *
     * @return ProductDto|Exception
     */
    public function current();

    /**
     * Return the key of the current element
     * @link https://php.net/manual/en/iterator.key.php
     * @return string|float|int|bool|null scalar on success, or null on failure.
     */
    public function key();

    /**
     * Move forward to next element
     * @link https://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next(): void;

    /**
     * Checks if current position is valid
     * @link https://php.net/manual/en/iterator.valid.php
     * @return bool The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid(): bool;

    /**
     * Rewind the Iterator to the first element
     * @link https://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind(): void;

    /**
     * Count elements of an object
     * @link https://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count(): int;
}

<?php

namespace LocalExpress\Processing\Bundles\FileReader\Interfaces;

use LocalExpress\Processing\Bundles\FileReader\Exceptions\NotFoundAnyAllowedParamException;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\NotFoundRequiredColumnException;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\NotFoundRequiredParamException;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\TableWithoutHeadersCouldNotBeenValidatedException;

/**
 * Interface FileReaderTableValidatorInterface
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Interfaces
 */
interface FileReaderTableValidatorInterface
{
    /**
     * FileReaderTableValidatorInterface constructor.
     *
     * @param FileReaderTableValidatorRulesInterface $rules
     */
    public function __construct(FileReaderTableValidatorRulesInterface $rules);

    /**
     * Method getter for preset rules.
     *
     * @return FileReaderTableValidatorRulesInterface
     */
    public function getRules():FileReaderTableValidatorRulesInterface;

    /**
     * Method pre-validate object and columns with required by rules.
     *
     * @param FileReaderTableLazyInterface $reader
     *
     * @return FileReaderTableValidatorInterface
     *
     * @throws TableWithoutHeadersCouldNotBeenValidatedException
     * @throws NotFoundRequiredColumnException
     */
    public function validate(FileReaderTableLazyInterface $reader): FileReaderTableValidatorInterface;

    /**
     * Method validate one row with allowed columns.
     *
     * @param $row
     *
     * @return FileReaderTableValidatorInterface
     *
     * @throws NotFoundRequiredParamException
     */
    public function validateRow(array $row): FileReaderTableValidatorInterface;

    /**
     * Method return all columns what allowed by rules.
     *
     * @return array
     *
     * @throws NotFoundAnyAllowedParamException
     */
    public function validatedColumns(): array;
}

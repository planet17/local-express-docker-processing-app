<?php

namespace LocalExpress\Processing\Bundles\FileReader\Interfaces;

use Generator;

/**
 * Interface FileReadersServiceInterface
 *
 * @package LocalExpress\Processing\Services\FileReaders
 */
interface FileReaderTableLazyInterface extends FileReaderBaseInterface
{
    /**
     * Method predicate return result has or no headers at table at file.
     *
     * @return bool
     *
     * @see FileReaderTableLazyInterface::setFlagSeparateHeaders() - method for change value from outside
     */
    public function hasHeaders():bool;

    /**
     * Method change flag return at FileReaderTableServiceInterface::hasHeaders()
     *
     * @param bool $flag
     *
     * @return FileReaderTableLazyInterface
     *
     * @see FileReaderTableLazyInterface::hasHeaders() - affected method
     */
    public function setFlagSeparateHeaders(bool $flag = false): FileReaderTableLazyInterface;

    /**
     * Method return list of column headers.
     *
     * @return array
     */
    public function getHeaders():array;

    /**
     * Method return list of records from table.
     *
     * @return null|Generator
     */
    public function getRows(): ?Generator;

    /**
     * Method return list of records from table mapped with key of columns from headers.
     *
     * @param string ...$columns
     *
     * @return null|Generator
     */
    public function getMappedRows(string ...$columns);

    /**
     * Setter method for rows.
     *
     * @param $rows
     *
     * @return FileReaderTableLazyInterface
     */
    public function setRows($rows): FileReaderTableLazyInterface;

    /**
     * Method count rows of table (without headers row).
     *
     * @return int
     */
    public function countRows(): int;
}

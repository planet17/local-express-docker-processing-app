<?php

namespace LocalExpress\Processing\Bundles\FileReader\Interfaces;

/**
 * Class FileReaderStrategyInterface
 *
 * That interface describing something like a strategy pattern.
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Interfaces
 */
interface FileReaderTypeBaseInterface extends FileReaderBaseInterface
{
    /**
     * FileReaderTypeInterface constructor.
     *
     * @param FileReaderTableLazyInterface $client
     */
    public function __construct(FileReaderTableLazyInterface $client);
}

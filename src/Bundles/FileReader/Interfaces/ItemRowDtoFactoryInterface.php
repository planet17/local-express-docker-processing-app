<?php

namespace LocalExpress\Processing\Bundles\FileReader\Interfaces;

use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\DTO\ProductDto;

/**
 * Interface ItemRowDtoFactoryInterface
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Interfaces
 */
interface ItemRowDtoFactoryInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function makeFrom(array $data);
}
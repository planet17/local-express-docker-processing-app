<?php

namespace LocalExpress\Processing\Bundles\FileReader\Exceptions;

use LengthException;

/**
 * Class NotFoundAnyAllowedParamException
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Exceptions
 */
class NotFoundAnyAllowedParamException extends LengthException
{
}

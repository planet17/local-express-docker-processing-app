<?php

namespace LocalExpress\Processing\Bundles\FileReader\Exceptions;

use InvalidArgumentException;

/**
 * Class TableWithoutHeadersCouldNotBeenValidatedException
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Exceptions
 */
class TableWithoutHeadersCouldNotBeenValidatedException extends InvalidArgumentException
{
}

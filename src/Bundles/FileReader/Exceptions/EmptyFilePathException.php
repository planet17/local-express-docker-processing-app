<?php

namespace LocalExpress\Processing\Bundles\FileReader\Exceptions;

/**
 * Class EmptyFilePathException
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Exceptions
 */
class EmptyFilePathException extends \InvalidArgumentException
{
}

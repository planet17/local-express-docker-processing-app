<?php

namespace LocalExpress\Processing\Bundles\FileReader\Exceptions;

use RuntimeException;

/**
 * Class ValidationColumnOrFactoryMakingException
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Exceptions
 */
class ValidationColumnOrFactoryMakingException extends RuntimeException
{
}

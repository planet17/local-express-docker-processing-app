<?php

namespace LocalExpress\Processing\Bundles\FileReader\Exceptions;

use LogicException;

/**
 * Class HasNoHeadersException
 *
 * @package LocalExpress\Processing\Services\FileReader
 */
class HasNoHeadersException extends LogicException
{
}

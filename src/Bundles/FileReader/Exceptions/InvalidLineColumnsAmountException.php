<?php

namespace LocalExpress\Processing\Bundles\FileReader\Exceptions;

use RuntimeException;

/**
 * Class InvalidRowColumnsAmountException
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Exceptions
 */
class InvalidLineColumnsAmountException extends RuntimeException
{
}

<?php

namespace LocalExpress\Processing\Bundles\FileReader\Exceptions;

/**
 * Class InvalidFilePathException
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Exceptions
 */
class InvalidFilePathException extends EmptyFilePathException
{
}

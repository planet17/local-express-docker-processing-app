<?php

namespace LocalExpress\Processing\Bundles\FileReader\Exceptions;

use LogicException;

/**
 * Class LazyCollectionEmptyException
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Exceptions
 */
class LazyCollectionEmptyException extends LogicException
{
}

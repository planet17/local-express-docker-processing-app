<?php

namespace LocalExpress\Processing\Bundles\FileReader\Exceptions;

use RuntimeException;
use Throwable;

/**
 * Class UndefinedFileExtensionProcessingStrategyException
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Exceptions
 */
class UndefinedFileExtensionProcessingStrategyException extends RuntimeException
{
    /** @const DEFAULT_MESSAGE_PREFIX string */
    public const DEFAULT_MESSAGE_PREFIX = 'No strategy for provided extension: ';

    /**
     * UndefinedFileExtensionProcessingStrategyException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct('No strategy for provided extension: ' . $message, $code, $previous);
    }
}

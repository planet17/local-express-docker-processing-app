<?php

namespace LocalExpress\Processing\Bundles\FileReader\Exceptions;

use LogicException;

/**
 * Class EmptyHeadersException
 *
 * @package LocalExpress\Processing\Services\FileReader
 */
class EmptyHeadersException extends LogicException
{
}

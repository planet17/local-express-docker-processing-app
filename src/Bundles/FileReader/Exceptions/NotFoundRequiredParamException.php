<?php

namespace LocalExpress\Processing\Bundles\FileReader\Exceptions;

use InvalidArgumentException;
use Throwable;

/**
 * Class NotFoundRequiredParamException
 *
 * @package LocalExpress\Processing\Bundles\FileReader\Exceptions
 */
class NotFoundRequiredParamException extends InvalidArgumentException
{
    /** @const DEFAULT_MESSAGE_PREFIX string */
    public const DEFAULT_MESSAGE_PREFIX = 'Not found required param: ';

    /**
     * NotFoundRequiredParamException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct(self::DEFAULT_MESSAGE_PREFIX . $message, $code, $previous);
    }
}

<?php

namespace LocalExpress\Processing\Bundles\FileReader\Readers\Strategies;

use League\Csv\AbstractCsv;
use League\Csv\Reader;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\FileReaderTableLazyInterface;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\FileReaderTypeBaseInterface;

/**
 * Class CsvLeagueStrategy
 *
 * @package LocalExpress\Processing\Services\FileReader
 */
class CsvLeagueStrategy implements FileReaderTypeBaseInterface
{
    /** @var AbstractCsv|Reader|null $reader */
    protected $reader = null;

    /** @var FileReaderTableLazyInterface */
    private $client;

    /** @inheritdoc */
    public function __construct(FileReaderTableLazyInterface $client)
    {
        $this->client = $client;
        $this->ensureNoPlatformsBug();
    }

    /** @inheritdoc */
    public function handleFile(string $filePath): void
    {
        $this->reset();
        $this->reader = Reader::createFromPath($filePath, 'r');
        $this->reader->setDelimiter($this->getCustomDelimiter());
        $this->ensureUTF();
        $this->fetchData();
    }

    /** Method reset reader, etc. before start handle new file */
    protected function reset(): void
    {
        $this->reader = null;
    }

    /**
     * Getter symbol delimiter.
     *
     * @return string
     */
    public function getCustomDelimiter(): string
    {
        return ',';
    }

    /**
     * That directive helps works with file updated at MacOS system or whether app run from MacOS.
     *
     * @void
     */
    protected function ensureNoPlatformsBug(): void
    {
        if (!ini_get('auto_detect_line_endings')) {
            ini_set('auto_detect_line_endings', '1');
        }
    }

    /** Check UTF16 and convert it to UTF8 */
    protected function ensureUTF(): void
    {
        $inputBom = $this->reader->getInputBOM();
        if ($inputBom === Reader::BOM_UTF16_LE || $inputBom === Reader::BOM_UTF16_BE) {
            $this->reader->appendStreamFilter('convert.iconv.UTF-16/UTF-8');
        }
    }

    /**
     * Method execute fetch data from table.
     */
    private function fetchData(): void
    {
        $this->client->setRows($this->reader->fetchAll());
    }
}

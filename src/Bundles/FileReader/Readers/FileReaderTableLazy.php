<?php

namespace LocalExpress\Processing\Bundles\FileReader\Readers;

use Generator;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\EmptyFilePathException;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\EmptyHeadersException;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\HasNoHeadersException;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\InvalidFilePathException;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\InvalidLineColumnsAmountException;
use LocalExpress\Processing\Bundles\FileReader\Exceptions\UndefinedFileExtensionProcessingStrategyException;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\FileReaderTableLazyInterface;
use LocalExpress\Processing\Bundles\FileReader\Interfaces\FileReaderTypeBaseInterface;
use LocalExpress\Processing\Bundles\FileReader\Readers\Strategies\CsvLeagueStrategy;
use SplFileInfo;

/**
 * Class FileReaderTableBase
 *
 * Director class for fetching data from file
 *
 * @package LocalExpress\Processing\Services\FileReader
 */
class FileReaderTableLazy implements FileReaderTableLazyInterface
{
    /** @var bool */
    protected $hasHeaders = false;

    /** @var array */
    protected $headers = [];

    /** @var array */
    protected $rows = [];

    /** @var FileReaderTypeBaseInterface $strategy */
    protected $strategy;

    /**
     * @return bool
     */
    public function hasHeaders(): bool
    {
        return $this->hasHeaders;
    }

    /** @inheritdoc  */
    public function setFlagSeparateHeaders(bool $flag = false): FileReaderTableLazyInterface
    {
        $this->hasHeaders = $flag;

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /** @inheritdoc */
    public function getMappedRows(string ...$columns)
    {
        $this->ensureHeadersExist();
        $mapped = iterator_to_array($this->resolveColumnPositions($columns));
        foreach ($this->rows as $line => $row) {
            if (count($this->getHeaders()) !== count($row)) {
                /* Return exception for concrete line instead of throwing */
                yield new InvalidLineColumnsAmountException(
                    'Row columns amount not equal to headers on line: ' . ($line + 1)
                );

                continue;
            }

            $item = [];
            foreach ($mapped as $name => $position) {
                $item[$name] = $row[$position];
            }

            yield $item;
        }
    }

    /**
     * Method resolve array with name of columns mapped by positions.
     *
     * @param array $columns
     *
     * @return Generator
     */
    private function resolveColumnPositions(array $columns): Generator
    {
        /* get all columns whether it is empty */
        if (!$columns) {
            $columns = $this->getHeaders();
        }

        $flip = array_flip($this->getHeaders());
        foreach ($columns as $name) {
            yield $name => $flip[$name];
        }
    }

    /** @inheritdoc */
    public function getRows(): ?Generator
    {
        foreach ($this->rows as $row) {
            yield $row;
        }
    }

    /**
     * Method validate option and exists of headers.
     */
    protected function ensureHeadersExist(): void
    {
        if (!$this->hasHeaders()) {
            throw new HasNoHeadersException('hasHeaders set to false');
        }

        if (!$this->getHeaders()) {
            throw new EmptyHeadersException('Headers is empty');
        }
    }

    /**
     * Method prepare all props for new clean reading file.
     */
    protected function reset(): void
    {
        $this->headers = [];
        $this->rows = [];
    }

    /** @inheritdoc */
    public function setRows($rows): FileReaderTableLazyInterface
    {
        if ($this->hasHeaders()) {
            $this->headers = array_shift($rows);
        }

        $this->rows = $rows;

        return $this;
    }

    /** @inheritdoc  */
    public function countRows(): int
    {
        return count($this->rows);
    }

    /** @inheritdoc */
    public function handleFile(string $filePath): void
    {
        if (!$filePath) {
            throw new EmptyFilePathException('File path must provided');
        }

        if (!file_exists($filePath)) {
            throw new InvalidFilePathException('Not found file by file path provided');
        }

        $this->reset();
        $this->resolveTypeByFilePath($filePath);
        $this->strategy->handleFile($filePath);
    }

    protected function resolveTypeByFilePath(string $filePath): void
    {
        $extension = (new SplFileInfo($filePath))->getExtension();
        switch ($extension) {
            case 'csv':
                $this->strategy = new CsvLeagueStrategy($this);
                break;
            default:
                throw new UndefinedFileExtensionProcessingStrategyException($extension);
        }
    }
}

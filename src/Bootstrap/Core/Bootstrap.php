<?php

namespace LocalExpress\Processing\Bootstrap\Core;

use LocalExpress\Processing\Bootstrap\Queue\QueueApp;
use Planet17\ApplicationProcessManagerRoutedQueue\Interfaces\ApplicationProcessManagerInterface;

/**
 * Class Core
 *
 * @package LocalExpress\Processing\Core\Core
 */
class Bootstrap
{
    /** @var ApplicationProcessManagerInterface */
    private $queueApp;

    /** Core constructor for set-up application. */
    public function __construct()
    {
        $this->queueApp = QueueApp::getInstance();
    }

    /**
     * Method for running main feature, handling requests, messages, etc.
     */
    public function run(): void
    {
        $this->initializeQueueHandling();
    }


    /**
     * Method for extracting some calls with instance containable object.
     */
    private function initializeQueueHandling(): void
    {
        $this->queueApp->initialize();
    }
}

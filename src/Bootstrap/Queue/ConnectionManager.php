<?php

namespace LocalExpress\Processing\Bootstrap\Queue;

use Planet17\MessageQueueLibrary\Connections\ConnectionFactory;
use Planet17\MessageQueueLibrary\Interfaces\Providers\RoutesProviderInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\ConnectionResolverInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\RouteResolverInterface;
use Planet17\MessageQueueLibrary\Resolvers\ConnectionResolver;
use Planet17\MessageQueueLibrary\Resolvers\RouteInstanceResolver;
use Planet17\MessageQueueLibraryRouteNav\Connections\ConnectionManagerBase;
use Planet17\MessageQueueLibraryRouteNav\Interfaces\Resolvers\AliasHandlerResolverInterface;
use Planet17\MessageQueueLibraryRouteNav\Resolvers\AliasHandlerResolver;

/**
 * Class ConnectionManager
 *
 * @package LocalExpress\Processing\Bootstrap\Queue
 */
class ConnectionManager extends ConnectionManagerBase
{
    /** @inheritdoc  */
    public function getResolverAliasHandler(): AliasHandlerResolverInterface
    {
        return new AliasHandlerResolver($this->getRoutesProvider(), new HandlersProvider);
    }

    /** @inheritdoc  */
    public function getResolverRouteInstance(): RouteResolverInterface
    {
        return new RouteInstanceResolver;
    }

    /** @inheritdoc  */
    public function getResolverConnection(): ConnectionResolverInterface
    {
        return new ConnectionResolver(new ConnectionFactory, $this);
    }

    /** @inheritdoc  */
    public function getRoutesProvider(): RoutesProviderInterface
    {
        return new RoutesProvider;
    }
}

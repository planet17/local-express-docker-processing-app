<?php

namespace LocalExpress\Processing\Bootstrap\Queue;

use LocalExpress\CommonQueueObjects\Bundles\ImportProcessing\Routes\ImportProcessingBalancerRoute;
use LocalExpress\CommonQueueObjects\Bundles\ProcessManager\Routes\ProcessManagerRoute;
use LocalExpress\Processing\Bundles\ImportProcessing\Routes\FileProcessingRoute;

/**
 * Class RoutesProvider
 *
 * @package LocalExpress\Processing\Bootstrap\Queue
 */
class RoutesProvider extends \Planet17\MessageQueueLibrary\Providers\RoutesProvider
{
    /** @inheritdoc */
    public function provideRouteClasses(): array
    {
        return [
            ProcessManagerRoute::class,
            ImportProcessingBalancerRoute::class,
            FileProcessingRoute::class,
        ];
    }

    /** @inheritdoc  */
    public function providePackageClasses(): array
    {
        return [
        ];
    }
}

<?php

namespace LocalExpress\Processing\Bootstrap\Queue;

use Planet17\ApplicationProcessManagerRoutedQueue\Core\ApplicationProcessManager;
use Planet17\MessageQueueLibraryRouteNav\Interfaces\Connections\ManagerInterface as ConnectionManagerInterface;

/**
 * Class QueueApp
 *
 * @package LocalExpress\Processing\Bootstrap\Queue
 */
class QueueApp extends ApplicationProcessManager
{
    /** @inheritdoc */
    public function getConnectionManager(): ConnectionManagerInterface
    {
        return ConnectionManager::getInstance();
    }

    /** @inheritdoc */
    public function fetchConfigurations(): array
    {
        return [
            (getenv('QUEUE_CONNECTION_NAME') ?: 'default') => [
                'driver' => getenv('QUEUE_DRIVER'),
                'host' => getenv('QUEUE_HOST'),
                'port' => getenv('QUEUE_PORT'),
            ],
        ];
    }
}

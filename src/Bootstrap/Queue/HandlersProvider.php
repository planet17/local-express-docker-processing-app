<?php

namespace LocalExpress\Processing\Bootstrap\Queue;

use LocalExpress\Processing\Bundles\ImportProcessing\Handlers\FileProcessingHandler;
use LocalExpress\Processing\Bundles\ImportProcessing\Handlers\ImportProcessingBalancerHandler;
use LocalExpress\Processing\Bundles\ProcessManager\ProcessManagerHandler;

/**
 * Class HandlersProvider
 *
 * @package LocalExpress\Processing\Bootstrap\Queue
 */
class HandlersProvider extends \Planet17\MessageQueueLibraryRouteNav\Providers\HandlersProvider
{
    /** @inheritdoc  */
    public function provideHandlerClasses(): array
    {
        return [
            ProcessManagerHandler::class,
            ImportProcessingBalancerHandler::class,
            FileProcessingHandler::class,
        ];
    }
}

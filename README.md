# Local Express: Processing #

https://bitbucket.org/planet17/local-express-docker-processing-app

Backend service for processing files or another hard works.
It doesn't contain any operations with storage, only php processes.


-------------------


Completed like Test Specification application.

Application based on pure PHP. That application is part of a few docker containers.
For more details about whole project look the repository
[Bridge](https://bitbucket.org/planet17/local-express-docker-core-bridge).

-------------------


## Environment ##


-------------------

    Docker: *
    PHP >= 7.3.25
    Ext: gearman, pcntl, posix
    Gearman (PECL): >=2.0.3


-------------------

## Installation ##

After cloning that project create local `.env` and create symlink (`ln -s`)
to for `/mount/cdn/` directory at docker container.
By docker-compose your real place for a link at `/.docker/mount/cdn`.
Directory for linking exist at project `Bridge` by path (`%bridge_project%/.docker/mount/cdn`).

```shell
cp .env.example .env
ln -s %current_project_path%/.docker/mount %bridge_project_path%/.docker/mount
```

If something from `.env` had been changed in another project then it must have the same value here is.
After syncing of `.env` file run following:


```shell
docker-compose up -d
docker-compose exec app composer install
```

## Project directories overview ##

-------------------

```
.docker                     images, settings, preset of configs, temporary files.
public                      contains file entry-point
src                         application
    Bootstap                application core for setup configurations and routes handlers, etc.
        Core/               common feature with app configuration
        Queue/              common lasses for configuration queue part of app
    Bundles                 bundle (module) - separated package of feature with area of responsibility
       FileReader/          bundle for works with files and documents.
       ImportProcessing/    bundle of processing document importing store products
       ProcessManager/      bundle for managing queue handlers
```

-------------------

### Non project but also included ###

#### Domain library ####

[Dashboard for Gearman](https://bitbucket.org/planet17/local-express-common-queue-objects) - library
uses at both application (`Processing` and `Yii2`). It contains some common elements for message queue.

#### Non domain libraries but written by me for works with message queue: ####

* [https://bitbucket.org/planet17/message-queue-library](https://bitbucket.org/planet17/message-queue-library)
* [https://bitbucket.org/planet17/message-queue-library-route-nav](https://bitbucket.org/planet17/message-queue-library-route-nav)
* [https://bitbucket.org/planet17/message-queue-library-process-manager](https://bitbucket.org/planet17/message-queue-library-process-manager)
* [https://bitbucket.org/planet17/cli-processes](https://bitbucket.org/planet17/cli-processes)
* [https://bitbucket.org/planet17/application-process-manager-routed-queue](https://bitbucket.org/planet17/application-process-manager-routed-queue)
* [https://bitbucket.org/planet17/message-queue-library-pipeline-handlers](https://bitbucket.org/planet17/message-queue-library-pipeline-handlers)


-------------------


### Bundle what works with queues ###

It contains at least elements such as Handlers, Messages, and their Routes, maybe DTO for provide data via messages or 
layers.


-------------------

```
Handlers/       logic for handling messages
Messages/       interface for quickly create new message at queue
Routes/         like a configuration aliases and connections (use it for Messages and Handlers)
                for help set-up them
```

-------------------

